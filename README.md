# Product Landing Page
Example of product landing page. This project uses HTML and pure CSS.

https://discrimy.gitlab.io/product-landing-page/

> Created during **[freeCodeCamp](https://freecodecamp.org)** learning from scratch
